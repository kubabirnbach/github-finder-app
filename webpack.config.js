const path = require("path"),
  webpack = require("webpack"),
  HtmlWebpackPlugin = require("html-webpack-plugin"),
  MiniCssExtractPlugin = require('mini-css-extract-plugin'),
  OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
  TerserPlugin = require('terser-webpack-plugin'),
  UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const config = {
  context: path.resolve(__dirname, "src"),

  entry: {
    app: ["@babel/polyfill", "./index.js"],
  },

  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "./assets/js/[name].bundle.js",
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: /src/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              [
                "@babel/preset-env",
                {
                  targets: {
                    browsers: ["last 1 version", "ie >= 11"],
                  },
                },
              ],
            ],
          },
        },
      },
      {
        test: /\.html$/,
        use: ["html-loader"],
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(jpg|png|gif|svg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "./assets/media/",
            },
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ["file-loader"],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin({ cleanAfterEveryBuildPatterns: ['dist']}),
    new HtmlWebpackPlugin({ template: "index.html" }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  ],

  devServer: {
    contentBase: path.resolve(__dirname, "./dist/assets/media"),
    compress: true,
    port: 2000,
    stats: "errors-only",
    open: true,
  },

  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
      extractComments: false,
    }), 
    new OptimizeCSSAssetsPlugin(),
    new UglifyJsPlugin()
  ],
  },

  devtool: "inline-source-map",
};

module.exports = config;

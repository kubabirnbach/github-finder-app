import './assets/scss/app.scss';
import Loader from './components/Loader';
import Navbar from './components/Navbar'
import UserProfile from './components/UserProfile';
import UserTimeline from './components/UserTimeline';


export class App {
  initializeApp() {
    const navBar = new Navbar();
    const userProfile = new UserProfile();
    const userTimeline = new UserTimeline();
    const loader = new Loader();

    navBar.render();
    userProfile.render();
    userTimeline.render();
    loader.render();
  }
}

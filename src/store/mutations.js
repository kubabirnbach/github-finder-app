import GitHubApi from "../api/github";
import { filterEvent } from "../model/event";
import store from "./index"

export default {
  fetchUserData: async (state, payload) => {
    store.dispatch("updateLoadingStatus", "pending");

    try {
      const { data: userProfile } = await GitHubApi.getUserProfile(payload);
      const { data: eventsHistory } = await GitHubApi.getUserHistory(payload);

      state.userData = { userProfile, eventsHistory: filterEvent(eventsHistory) }
      store.dispatch("updateLoadingStatus", "success");
      return state;
    } catch (error) {
      store.dispatch("updateLoadingStatus", "error");
    }
  },
  updateLoadingStatus: (state, payload) => {
    state.loadingStatus = payload;
    return state;
  },
};

export default {
  userData: {
    userProfile: {
      avatar_url: "http://placekitten.com/200/200",
      bio:"",
      html_url: "",
      login: "johnsmith",
      name: "John Smith",
    },
    eventsHistory: [],
  },
  loadingStatus: "idle",
};

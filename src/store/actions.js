
export default {
    fetchUserData(context, payload) {
        context.commit('fetchUserData', payload);
    },
    updateLoadingStatus(context, payload) {
        context.commit('updateLoadingStatus', payload);
    },
};
export default {
    isNotEmpty: (str) => str.trim().length > 0,
    isValidName: (str) => /^[a-z0-9_\-]+$/.test(str)
}
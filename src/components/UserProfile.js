import Component from "../lib/component.js";
import store from "../store/index.js";
export default class UserProfile extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector(".profile-container"),
    });
  }

  render() {
    const self = this;
    const { userProfile } = store.state.userData;

    const biography = userProfile.bio || "User has no biography added"

    self.element.innerHTML = `
        <div class="profile">
          <div class="media">
            <div class="media-left">
              <figure class="media-left image is-64x64">
                <img src=${userProfile.avatar_url} id="profile-image">
              </figure>
            </div>
            <div class="media-content">
              <p class="title is-5" id="profile-name">${userProfile.name}</p>
              <p class="subtitle is-6">
                <a href=${userProfile.html_url} id="profile-url" target="_blank">
                  @${userProfile.login}
                </a>
              </p>
            </div>
          </div>


          <div class="content" id="profile-bio">
            <p>
              ${biography}
            </p>
          </div>
        </div>
    `;
  }
}

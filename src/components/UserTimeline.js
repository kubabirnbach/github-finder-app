import Component from "../lib/component.js";
import store from "../store/index.js";
import { dateFormat } from "../utils/dateFormat.js";

export default class UserTimeline extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector("#user-timeline"),
    });
  }

  render() {
    const self = this;
    const { eventsHistory } = store.state.userData;

    if (eventsHistory.length === 0) {
      self.element.innerHTML = `<p class="no-items">This user hasn't done anything recently 😢</p>`;
      return;
    }

    const eventList = eventsHistory
      .map((event) => {
        if (event.type === "PullRequestEvent") {
          return `
              <div class="timeline-item">
                <div class="timeline-marker"></div>
                <div class="timeline-content" style="width: 100%">
                <div class="columns">
                  <div class="column is-full-desktop is-7-widescreen">
                    <div class="box">
                        <article class="media">
                          <div class="media-left">
                            <figure class="image is-64x64">
                              <img src=${event.payload.pull_request.user.avatar_url}/>
                            </figure>
                          </div>
                          <div class="media-content">
                              <p class="heading">${ dateFormat(event.created_at) }</p>
                                    <span class="gh-username">
                                      <a href=${event.payload.pull_request.user.url}>${event.payload.pull_request.user.login}</a>
                                    </span>
                                ${event.payload.action}
                                <a href=${event.payload.pull_request.url}>pull request</a>
                                <p class="repo-name">
                                  <a href=${event.repo.url}>${event.repo.name}</a>
                                </p>
                          </div>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          `;
        }

        if (event.type === "PullRequestReviewCommentEvent") {
          return `
            <div class="timeline-item">
              <div class="timeline-marker"></div>
              <div class="timeline-content" style="width: 100%">
              <div class="columns">
                  <div class="column is-full-desktop is-7-widescreen">
                    <div class="box">
                      <article class="media">
                        <div class="media-left">
                          <figure class="image is-64x64">
                            <img src=${event.payload.comment.user.avatar_url}/>
                          </figure>
                        </div>
                        <div class="media-content">
                          <p class="heading">${ dateFormat(event.created_at) }</p>
                          <div class="content">
                            <span class="gh-username">
                              <a href=${event.payload.comment.url}>${event.payload.comment.user.login}</a>
                            </span>
                            ${event.payload.action}
                            <a href=${event.payload.comment.html_url}>comment</a>
                            to
                            <a href=${event.payload.comment.url}>pull request</a>
                            <p class="repo-name">
                              <a href=${event.repo.url}>${event.repo.name}</a>
                            </p>
                          </div>
                      </article>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          `;
        }
      })
      .join("");

    self.element.innerHTML = eventList;
  }
}

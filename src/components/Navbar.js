import $ from "cash-dom";
import Component from "../lib/component.js";
import store from "../store/index.js";
import validator from "../utils/validator";

export default class Navbar extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector(".navbar"),
    });
  }

  _addSubmitEventListener() {
    $(".load-username").on("click", () => {
      const userName = $("#username-form").val();

      if (!validator.isNotEmpty(userName) || !validator.isValidName(userName)) {
        return $("#username-form").addClass("is-danger");
      }

      store.dispatch("fetchUserData", userName);

      $("#username-form").val("");
    });
  }

  _addCleanEventListener() {
    $("#username-form").on("focus", () => {
      if ($("#username-form").hasClass("is-danger")) {
        $("#username-form").removeClass("is-danger");
      }
    });
  }

  render() {
    let self = this;

    const profileNotFound = store.state.profileNotFound ? "" : "is-hidden";

    self.element.innerHTML = `
      <div class="container">
        <div class="navbar-brand">
          <div class="navbar-item">
            <div class="field has-addons">
              <div class="control">
                <input class="input username" type="text" placeholder="enter github username" id='username-form'>
              </div>
              <div class="control">
                <button class="button is-info load-username">
                  Load
                </button>
              </div>
            </div>
          </div>
          <div class="navbar-item ${profileNotFound}">
            <span class="tag is-danger">Nie znaleziono profilu</span>
          </div>
          <div class="navbar-item">
            <div class="loader is-hidden" id="spinner"></div>
          </div>
        </div>
      </div>
    `;

    this._addSubmitEventListener();
    this._addCleanEventListener();
  }
}

import $ from "cash-dom";
import Component from "../lib/component.js";
import store from "../store/index.js";

export default class Loader extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector("#loader"),
    });
  }

  _updateLoader() {
    const { loadingStatus } = store.state;

    if (loadingStatus === "pending") {
      $("#loader").removeClass("is-hidden");
      $("#error").addClass("is-hidden");
      $("#user-profile").addClass("is-hidden");
      $("#user-history").addClass("is-hidden");
      return;
    }

    if (loadingStatus === "success") {
      $("#user-profile").removeClass("is-hidden");
      $("#user-history").removeClass("is-hidden");
      $("#loader").addClass("is-hidden");
      $("#error").addClass("is-hidden");
      return;
    }

    if (loadingStatus === "error") {
      $("#user-profile").addClass("is-hidden");
      $("#user-history").addClass("is-hidden");
      $("#loader").addClass("is-hidden");
      $("#error").removeClass("is-hidden");
      return;
    }
  }

  render() {
    const self = this;

    self.element.innerHTML = `
      <div class="loading">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      </div>
    `;
    self._updateLoader();
  }
}

import axios from 'axios';

axios.defaults.baseURL = 'https://api.github.com/users'

const getUserProfile = (name) => axios.get(`/${name}`)
const getUserHistory = (name) => axios.get(`/${name}/events/public`)

export default {
    getUserProfile,
    getUserHistory
}
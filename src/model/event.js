const availableEventTypes = ['PullRequestEvent', 'PullRequestReviewCommentEvent']; 

export const filterEvent = (events) => events.filter(event => availableEventTypes.includes(event.type))